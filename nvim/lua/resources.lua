-- vim.api.nvim_set_hl(0, "norm", { bg = "#00FFFF", fg = "#253437" })
-- -- insert hlgroup
-- vim.api.nvim_set_hl(0, "insert", { bg = "#4ff97b", fg = "#253437" })
-- -- visual
-- vim.api.nvim_set_hl(0, "visual", { bg = "#c893eb", fg = "#253437" })
-- -- replace
-- vim.api.nvim_set_hl(0, "replace", { bg = "#f07279", fg = "#253437" })
-- -- command
-- vim.api.nvim_set_hl(0, "command", { bg = "#FFD700", fg = "#253437" })
-- yellow highlight group
-- local test = "%#COLOR#STRING%##"

hl_colors = {
    aqua = "#00FFFF",
    black = "#000000",
    blue = "#0000FF",
    blue2 = "#2144e1",
    blue_windows = "#009aea",
    darkaqua = "#00b2b2",
    darkergrey = "#252525",
    darkerblue = "#171826",
    darkgold = "#B9960B",
    darkgreen = "#006400",
    darkgrey = "#353535",
    darkorange = "#FF6600",
    darkpurple = "#6600FF",
    darkred = "#800000",
    darkwhite = "#D0D0D0",
    darkyellow = "#d19f02",
    fuchsia = "#FF00FF",
    gold = "#FFD700",
    green1 = "#008000",
    green = "#00FF00",
    green3 = "#32CD32",
    grey = "#808080",
    indigo = "#4b0082",
    softindigo ="#997dff",
    navy = "#000080",
    navyblue = "#0078d7",
    offwhite = "#F8F8F8",
    olive = "#808000",
    orange = "#FFA500",
    orange2 = "#984500",
    pink = "#FFC0CB",
    purple = "#886ce4",
    red = "#FF0000",
    red2 = "#980000",
    silver = "#C0C0C0",
    softblue = "#7db3ff",
    softblue2 = "#65a8ff",
    softgreen = "#8fff7d",
    softorange = "#ffb565",
    softred = "#ff6865",
    softyellow = "#fffa65",
    teal = "#008080",
    violet = "#EE82EE",
    white = "#FFFFFF",
    yellow = "#FFFF00",
    yellow2 = "#fed136",
    yellow3 = "#745802",
    yellow4 = "#877c00",
    NONE = "NONE",
}

function get_hl_color(color)
    return hl_colors[color]
end

-- function color_string(color, str)
--     vim.api.nvim_set_hl(0, color, { fg = get_hl_color(color), bg = "NONE" })
--     colorpick = color
--     -- Passes this: "%#COLOR#STRING%##"
--     local x1 = "%#" .. colorpick .. "#" .. str .. "%##"
--     return x1
-- end

function color_string(name, color, str)
    -- vim.api.nvim_set_hl(0, name, { fg = get_hl_color(color) })
    vim.api.nvim_set_hl(0, name, { fg = get_hl_color(color), bg = "NONE" })
    -- Passes this: "%#HLNAME#STRING%##"
    local x1 = "%#" .. name .. "#" .. str .. "%##"
    return x1
end

function color_string2(name, color1, color2, str)
    vim.api.nvim_set_hl(0, name, { fg = get_hl_color(color1), bg = get_hl_color(color2) })
    -- Passes this: "%#HIGHLIGHTNAME#STRING%##"
    x1 = "%#" .. name .. "#" .. str .. "%##"
    return x1
end

ignore_list = {
    "terminal",
    "toggleterm",
    "command line"
}
-- 0=============================================================================================0
filetype_icons = {
    lua = "🔵",
    python = "🐍",
    txt = "📄",
}

-- 0=============================================================================================0
mode_convert = {
    ["n"] = "NORMAL",
    ["v"] = "VISUAL",
    ["\22"] = "V-BLOCK",
    ["s"] = "SELECT",
    ["S"] = "SELECT",
    ["\19"] = "SELECT",
    ["i"] = "INSERT",
    ["R"] = "REPLACE",
    ["c"] = "COMMAND",
    ["r"] = "PROMPT",
    ["r?"] = "CONFIRM",
    ["!"] = "SHELL",
    ["t"] = "TERMINAL",
}
-- 0=============================================================================================0
mode_convert1 = {
    ["n"] = "NORMAL",
    ["no"] = "OP-PENDING",
    ["nov"] = "OP-PENDING",
    ["noV"] = "OP-PENDING",
    ["no\22"] = "OP-PENDING",
    ["niI"] = "I-NORMAL",
    ["niR"] = "NORMAL",
    ["niV"] = "NORMAL",
    ["nt"] = "NORMAL",
    ["ntT"] = "NORMAL",
    ["v"] = "VISUAL",
    ["vs"] = "VISUAL",
    ["V"] = "V-LINE",
    ["Vs"] = "V-LINE",
    ["\22"] = "V-BLOCK",
    ["\22s"] = "V-BLOCK",
    ["s"] = "SELECT",
    ["S"] = "SELECT",
    ["\19"] = "SELECT",
    ["i"] = "INSERT",
    ["ic"] = "INSERT",
    ["ix"] = "INSERT",
    ["R"] = "REPLACE",
    ["Rc"] = "REPLACE",
    ["Rx"] = "REPLACE",
    ["Rv"] = "VIRT REPLACE",
    ["Rvc"] = "VIRT REPLACE",
    ["Rvx"] = "VIRT REPLACE",
    ["c"] = "COMMAND",
    ["cv"] = "VIM EX",
    ["ce"] = "EX",
    ["r"] = "PROMPT",
    ["rm"] = "MORE",
    ["r?"] = "CONFIRM",
    ["!"] = "SHELL",
    ["t"] = "TERMINAL",
}
-- 0=============================================================================================0
