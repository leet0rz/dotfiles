--TO DO LIST:
--0=============================================================================================0
-- █░░ ▄▀█ ▀█ █▄█
-- █▄▄ █▀█ █▄ ░█░
--0=============================================================================================0
-- Leader key
vim.g.mapleader = " "
local rm = vim.keymap.set
-- Don't touch, lazy bootstrap.
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable", -- latest stable release
        lazypath,
    })
end
vim.opt.rtp:prepend(lazypath)
--0=============================================================================================0
-- █▀█ █░░ █░█ █▀▀ █ █▄░█ █▀   █░░ █ █▀ ▀█▀
-- █▀▀ █▄▄ █▄█ █▄█ █ █░▀█ ▄█   █▄▄ █ ▄█ ░█░
--0=============================================================================================0
require("lazy").setup({

    -- THEMES
    -- { "shaunsingh/moonlight.nvim" },
    -- { "tapsu01/moonlight.nvim" },
    { "leet0rz/moonlight.nvim" },
    -- {"folke/tokyonight.nvim"},
    -- {"catppuccin/nvim", name = "catppuccin", priority = 1000},

    {
        "EdenEast/nightfox.nvim",
        config = function()
            -- vim.cmd("colorscheme habamax")
            -- vim.cmd("colorscheme carbonfox")
            vim.cmd("colorscheme moonlight")
            -- vim.cmd("colorscheme tokyonight-moon")
            vim.cmd('hi StatusLine guibg=NONE')
            -- vim.cmd('hi Normal guibg=#101317')
        end
    },
    {
        "terrortylor/nvim-comment",
        config = function()
            require("nvim_comment").setup()
        end

    },
    {
        "windwp/nvim-autopairs",
        config = function()
            require("nvim-autopairs").setup()
        end
    },
    {
        "folke/flash.nvim",
        event = "VeryLazy",
        ---@type Flash.Config
        opts = {},
        -- stylua: ignore
        keys = {
            { "s", mode = { "n", "x", "o" }, function() require("flash").jump() end, desc = "Flash" },
            {
                "S",
                mode = { "n", "x", "o" },
                function() require("flash").treesitter() end,
                desc =
                "Flash Treesitter"
            },
            {
                "r",
                mode = "o",
                function() require("flash").remote() end,
                desc =
                "Remote Flash"
            },
            {
                "R",
                mode = { "o", "x" },
                function() require("flash").treesitter_search() end,
                desc =
                "Treesitter Search"
            },
            {
                "<c-s>",
                mode = { "c" },
                function() require("flash").toggle() end,
                desc =
                "Toggle Flash Search"
            },
        },
        config = function()
            vim.api.nvim_set_hl(0, 'FlashLabel', { fg = hl_colors.white })
        end
    },
    --0=============================================================================================0
    -- ▀█▀ █▀▀ █░░ █▀▀ █▀ █▀▀ █▀█ █▀█ █▀▀
    -- ░█░ ██▄ █▄▄ ██▄ ▄█ █▄▄ █▄█ █▀▀ ██▄
    --0=============================================================================================0
    {
        'nvim-telescope/telescope.nvim',
        tag = '0.1.2',
        dependencies = { 'nvim-lua/plenary.nvim' },
        config = function()
            require("telescope").setup {
                defaults = {
                    -- border = opts1,
                    sorting_strategy = "ascending",
                    layout_strategy = "horizontal",
                    layout_config = {
                        horizontal = {
                            prompt_position = "top",
                            preview_width = 0.5,
                            results_width = 0.5,
                            height = 0.5,
                            preview_cutoff = 120,
                        }
                    },
                },
            }
        end
    },
    {
        'nvim-telescope/telescope-fzf-native.nvim',
        build =
        'cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build',
        config = function()
            require("telescope").load_extension "fzf"
        end
    },
    --0=============================================================================================0
    -- █▀█ █ █░░
    -- █▄█ █ █▄▄
    --0=============================================================================================0
    {
        'stevearc/oil.nvim',
        config = function()
            -- vim.g.loaded_netrwPlugin = 1
            -- vim.g.loaded_netrw = 1
            require("oil").setup({
                default_file_explorer = true,
                keymaps = {
                    ["<BS>"] = "actions.parent",
                    ["<C-s>"] = ":w!<CR>",
                },
                prompt_save_on_select_new_entry = false,
                view_options = { show_hidden = true },
            })
        end
    },
    {
        'tzachar/local-highlight.nvim',
        config = function()
            require('local-highlight').setup({
                file_types = { 'python', 'cpp', "lua", "txt", "poefilter", "json" }, -- If this is given only attach to this
                -- OR attach to every filetype except:
                disable_file_types = { 'tex' },
                hlgroup = 'Search',
                cw_hlgroup = nil,
                -- Whether to display highlights in INSERT mode or not
                insert_mode = false,
            })
        end
    },
    --0=============================================================================================0
    -- ▀█▀ █▀▀ █▀ ▀█▀   ▀█ █▀█ █▄░█ █▀▀
    -- ░█░ ██▄ ▄█ ░█░   █▄ █▄█ █░▀█ ██▄
    --0=============================================================================================0
    {
        "NvChad/nvim-colorizer.lua",
        config = function()
            require("colorizer").setup {
                filetypes = {
                    -- lua = {names = false},
                    "*"
                },
                user_default_options = {
                    names = false,
                    mode = "background", -- Set the display mode.
                    -- mode = "foreground", -- Set the display mode.
                    -- mode = "virtualtext", -- Set the display mode.
                },
            }
        end
    },
    -- {
    --     'akinsho/toggleterm.nvim',
    --     version = "*",
    --     -- config = true,
    --     config = function()
    --         require('toggleterm').setup({
    --             size = 60,
    --             -- shading_factor = "-200",
    --             -- shading_factor = "200",
    --             -- direction = 'float',
    --             direction = 'vertical',
    --             -- shade_terminals = true,
    --             shade_terminals = false,
    --             -- shell = "zsh",
    --             shell = "pwsh.exe",
    --         })
    --     end
    -- },
    {
        "CRAG666/code_runner.nvim",
        -- config = true
        config = function()
            require('code_runner').setup({
                term = {
                    -- window size, this option is ignored if tab is true
                    size = 12,
                },
                filetype = {
                    -- python = "python3 -u",
                    python = "py -u",
                    lua = "lua",
                },
            })
        end
    },
    -- {
    --     "mbbill/undotree",
    --     config = function()
    --         undotree_setup()
    --     end
    -- },
    {
        'Exafunction/codeium.vim',
        config = function()
            rm('i', '<c-.>', function() return vim.fn['codeium#CycleCompletions'](-1) end, { expr = true })
        end,
        event = 'BufEnter',
    },
    {
        "brenton-leighton/multiple-cursors.nvim",
        config = function()
            require("multiple-cursors").setup()
        end
    },
    -- {
    --     'nvim-tree/nvim-web-devicons',
    --     config = function()
    --         require('nvim-web-devicons').setup()
    --     end
    -- },
    -- {
    --     "williamboman/mason.nvim",
    --     config = function()
    --         require("mason").setup()
    --     end
    -- },
    --0=============================================================================================0

    --0=============================================================================================0
    -- ▀█▀ █▀█ █▀▀ █▀▀ █▀ █░█ █ ▀█▀ ▀█▀ █▀▀ █▀█
    -- ░█░ █▀▄ ██▄ ██▄ ▄█ █▀█ █ ░█░ ░█░ ██▄ █▀▄
    --0=============================================================================================0
    -- TSUpdate not here, might break on updates. Be aware.
    {
        'nvim-treesitter/nvim-treesitter',
        config = function()
            require('nvim-treesitter.install').compilers = { "zig" }
            require('nvim-treesitter.configs').setup {
                -- ensure_installed = { "c", "python", "lua", "bash" },
                -- Install parsers synchronously (only applied to `ensure_installed`)
                sync_install = false,
                -- Automatically install missing parsers when entering buffer
                -- Recommendation: set to false if you don't have `tree-sitter` CLI installed locally
                auto_install = false,
                -- List of parsers to ignore installing (for "all")
                ignore_install = { "javascript" },
                -- highlight/disable list
                highlight = {
                    enable = true,
                    -- disable = {},
                    disable = { "poe_filter", "comment", "lua" },
                },
            }
        end
    },
    --0=============================================================================================0
    -- █░░ █▀ █▀█
    -- █▄▄ ▄█ █▀▀
    --0=============================================================================================0
    {
        'neovim/nvim-lspconfig',
        -- opts = { inlay_hints = { enabled = true }, },
        config = function()
            -- event = { "BufRead", "BufNewFile" }
            local lspconfig = require('lspconfig')
            local capabilities = vim.lsp.protocol.make_client_capabilities()
            capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)


            local custom_attach = function(client, bufnr)
                print("Lsp Attached.")
            end

            -- -- Floating diagnostic window on cursor
            -- vim.api.nvim_create_autocmd("CursorHold", {
            --     buffer = bufnr,
            --     callback = function()
            --         local opts = {
            --             focusable = false,
            --             close_events = { "BufLeave", "CursorMoved", "InsertEnter", "FocusLost" },
            --             border = "rounded",
            --             source = "always",
            --             prefix = " ",
            --             scope = "cursor",
            --         }
            --         vim.diagnostic.open_float(nil, opts)
            --     end,
            -- })

            -- C LANGUAGE SERVER
            lspconfig.clangd.setup({
                on_attach = custom_attach,
                capabilities = capabilities,
                settings = {
                    ccls = {
                        completion = {
                            filterAndSort = false
                        }
                    },
                    clangd = {
                        cmd = { 'clangd', '--background-index', '--header-insertion=iwyu' },
                        filetypes = { 'c', 'cpp', 'objc', 'objcpp' },
                    },
                }
            })


            -- █▀█ █▄█ ▀█▀ █░█ █▀█ █▄░█
            -- █▀▀ ░█░ ░█░ █▀█ █▄█ █░▀█
            -- https://neovim.discourse.group/t/pyls-and-jedi-language-server-100-cpu-usage/233/15
            -- lspconfig.pyright.setup({
            --     on_attach = custom_attach,
            --     capabilities = capabilities,
            --     settings = {
            --         python = {
            --             analysis = {
            --                 autoSearchPaths = true,
            --                 useLibraryCodeForTypes = true,
            --                 diagnosticMode = 'openFilesOnly',
            --                 -- diagnosticMode = 'none',
            --                 typeCheckingMode = 'off'
            --             },
            --         },
            --     },
            -- })
            lspconfig.jedi_language_server.setup({
                -- OTHER:
                on_attach = custom_attach,
                capabilities = capabilities,
                init_options = {
                    diagnostics = {
                        -- enable = false,
                        enable = true,
                        didOpen = true,
                        didChange = true,
                        didSave = true,
                    },
                },
            })
            -- █░░ █░█ ▄▀█
            -- █▄▄ █▄█ █▀█
            lspconfig.lua_ls.setup {
                on_attach = custom_attach,
                capabilities = capabilities,
                settings = {
                    Lua = {
                        -- NEW
                        runtime = {
                            version = "LuaJIT",
                        },
                        diagnostics = {
                            -- enable = true,
                            enable = false,
                            -- globals = { "vim", "bufnr" },
                        },
                        workspace = {
                            CheckThirdParty = false,
                            library = {
                              vim.env.VIMRUNTIME,
                            },
                            -- library = vim.api.nvim_get_runtime_file("", true),
                        },
                        telemetry = { enabled = false },
                    },
                },
            }
        end
    },
    --0=============================================================================================0
    -- █▀▀ █▀▄▀█ █▀█
    -- █▄▄ █░▀░█ █▀▀
    --0=============================================================================================0
    {
        'hrsh7th/nvim-cmp', -- Required
        event = "InsertEnter",
        dependencies = {
            { 'hrsh7th/cmp-nvim-lsp' }, -- Required
            {
                'L3MON4D3/LuaSnip',
                dependencies = { "rafamadriz/friendly-snippets" },
            },

            -- Other:
            {
                "williamboman/mason.nvim",
                config = function()
                    require("mason").setup()
                  end
            },
            'hrsh7th/cmp-path',
            -- 'hrsh7th/cmp-nvim-lua',
            -- "hrsh7th/cmp-buffer",

        },
        config = function()
            -- CMP
            local cmp = require 'cmp'
            local cmp_ap = require('nvim-autopairs.completion.cmp')
            local luasnip = require 'luasnip'
            -- luasnip.filetype_extend("py")
            -- require('luasnip.loaders.from_vscode').lazy_load()
            luasnip.config.setup {}
            cmp.setup({
                snippet = {
                    -- REQUIRED - you must specify a snippet engine
                    expand = function(args)
                        require('luasnip').lsp_expand(args.body) -- For `luasnip` users.
                    end,
                },
                window = {
                    completion = cmp.config.window.bordered(),
                    documentation = cmp.config.window.bordered()
                },
                mapping = cmp.mapping.preset.insert({
                    ['<C-b>'] = cmp.mapping.scroll_docs(-4),
                    -- ['<C-Up>'] = cmp.mapping.scroll_docs(-4),
                    ['<C-f>'] = cmp.mapping.scroll_docs(4),
                    -- ['<C-Down>'] = cmp.mapping.scroll_docs(4),
                    -- ['<C-Space>'] = cmp.mapping.complete(),
                    ["<Esc>"] = cmp.mapping.abort(),
                    ["<CR>"] = cmp.mapping.confirm({ select = true }),
                }),
                sources = cmp.config.sources({
                    { name = "nvim_lsp" },                    --, keyword_length = 2 },
                    { name = "luasnip" },                     -- For luasnip users.
                    { name = "path",    keyword_length = 2 }, -- For luasnip users.
                    -- { name = "nvim_lua", keyword_length = 2 }, -- For vsnip users.
                    -- { name = "buffer", keyword_length = 4 }, -- For luasnip users.
                }),

            })
            -- bracket completion for lua
            cmp.event:on(
                'confirm_done',
                cmp_ap.on_confirm_done()
            )
        end
    },
})

-- OTHER
--- Diagnotics
vim.diagnostic.config({
    underline = false,
    severity_sort = true,
})
-- vim.api.nvim_create_autocmd({ "FocusLost", "BufLeave" }, {
--     callback = function()
--         local buftype = vim.bo.buftype
--
--         if buftype ~= 'nofile' and buftype ~= 'acwrite' then
--             vim.cmd("silent wa!")
--         end
--     end,
-- })
--
-- -- Highlight yanked text for 150ms
-- vim.api.nvim_create_autocmd("TextYankPost", {
--     callback = function()
--         vim.highlight.on_yank({ timeout = 150 })
--     end
-- })
--
--
-- vim.api.nvim_create_autocmd("BufWritePost", {
--     callback = function()
--         local filetype = vim.bo.filetype
--         if filetype == "python" then
--             vim.cmd("silent! !Black %")
--         else
--             return
--         end
--     end
-- })
