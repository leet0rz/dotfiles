vim.api.nvim_create_autocmd({ "FocusLost", "BufLeave" }, {
    callback = function()
        local buftype = vim.bo.buftype

        if buftype ~= 'nofile' and buftype ~= 'acwrite' then
            vim.cmd("silent wa!")
        end
    end,
})

-- Highlight yanked text for 150ms
vim.api.nvim_create_autocmd("TextYankPost", {
    callback = function()
        vim.highlight.on_yank({ timeout = 150 })
    end
})


vim.api.nvim_create_autocmd("BufWritePost", {
    callback = function()
        local filetype = vim.bo.filetype
        if filetype == "python" then
            vim.cmd("silent! !Black %")
        else
            return
        end
    end
})
